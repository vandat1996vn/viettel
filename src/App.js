import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import ProtectedRoutes from "./router/protectedRoutes";
import React, { useEffect, useState } from "react";

import Admin from "./layout/admin/admin";
import Auth from "./page/login/auth";
import { router, user } from "./router/constants";
import Layout from "./layout/admin/layout";
// import ProtectedAuth from "./router/protecdAuth";
import { useDispatch, useSelector } from "react-redux";
import { Fragment } from "react";
import NotFound from "./page/admin/notFound/NotFound";
import ErrServer from "./page/admin/notFound/errServer";
import Lock from "./page/admin/notFound/Lock";
import { selectAuthActive } from "./features/login/authSlice";
import { getProfile , selectProfile } from "./features/user/userSlice";

function App() {
  const dispatch = useDispatch();
  // const [profile, setProfile] = useState();
  const profile = useSelector(selectProfile);
  const [isStaff, setIsStaff] = useState(false);

  const token = localStorage.getItem("access_token");

  const allRouter = router;
  const isUnknownRoutes = allRouter.find((route) =>
    window.location.href.includes(route)
  );

  console.log('profile', profile?.data?.isStaff);

  useEffect(() => {
    if (profile?.data?.isStaff === true) {
      setIsStaff(true)
    } else {
      setIsStaff(false)
    }
  }, [profile?.data?.isStaff])


  return (
    <>
      <Routes>
        <Route exact path="/" element={<Layout />}>
          <Route index element={<Navigate to="/admin" />} />
          <Route path="/login" element={<Auth />} />
          <Route path="admin" element={<ProtectedRoutes Component={Admin} />}>
          <Route exact index element={<Navigate to="/admin/dashboard" />} />
            {router.map((item, index) => (
              <Fragment key={index}>
                <Route
                  exact
                  path={item.path}
                  key={index}
                  element={item.component}
                />
              </Fragment>
            ))}
          </Route>
          <Route path="500" element={<ErrServer />} />
          <Route path="505" element={<Lock />} />
          {!isUnknownRoutes && <Route path="*" element={<NotFound />} />}
        </Route>
      </Routes>
    </>
  );
}

export default App;
