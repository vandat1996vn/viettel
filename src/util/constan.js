import {
  BiPieChartAlt,
  BiMicrophone,
  BiGroup,
  BiPodcast,
  BiLinkAlt,
  BiUnlink,
  BiLink,
  BiVolumeFull
} from "react-icons/bi";

export const narBar = [
  {
    name: "Bảng điều khiển",
    path: "/admin/dashboard",
    icons: <BiPieChartAlt />,
    staff: ["USER", "ALL", "STAFF"]
  },
  {
    name: "Ghi âm cuộc gọi",
    path: "/admin/recordcall",
    icons: <BiMicrophone />,
    staff: ["ALL", "USER"]
  },
  {
    name: "Thống kê Agent",
    path: "/admin/statisticalagent",
    icons: <BiGroup />,
    staff: ["ALL", "USER"]
  },
  {
    name: "Voice Mail",
    path: "/admin/VoiceMail",
    icons: <BiVolumeFull />,
    staff: ["ALL", "USER"]
  },
  {
    name: "Theo dõi online",
    path: "/admin/followonline",
    icons: <BiPodcast />,
    staff: ["ALL", "USER"]
  },
  {
    name: "Thống kê card",
    path: "/admin/statisticalcard",
    icons: <BiLink />,
    staff: ["STAFF", "USER"]
  },
  {
    name: "Card online",
    path: "/admin/cardonline",
    icons: <BiLinkAlt />,
    staff: ["STAFF", "USER"]
  },
  {
    name: "Card ofline",
    path: "/admin/cardofline",
    icons: <BiUnlink />,
    staff: ["STAFF", "USER"]
  }
];
