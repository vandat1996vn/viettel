import React, { useRef, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectProfile } from "../../features/user/userSlice";
import { FaUserCircle } from "react-icons/fa";
import { BiCaretDown } from "react-icons/bi";

const Header = ({ menu }) => {
  const headerRef = useRef(null);
  const profileHeader = useSelector(selectProfile);
  const [active, setActive] = useState(false);

  console.log("profile", profileHeader);

  const handleOpenBtn = (event) => {
    setActive(active === false ? true : false);
    event.stopPropagation();
  };

  useEffect(() => {
    function myFunction() {
      setActive(false);
    }

    document.addEventListener("click", myFunction);
  });

  const logout = async () => {
    await localStorage.clear();
    window.location.reload();
  };

  return (
    <div className="header-container" ref={headerRef}>
      <header className="header navbar">
        <div className="box-user__login">
          <h3 className="name-user__login">{profileHeader?.data?.username}</h3>
          <div
            className={`btn-avatar__header ${active === true ? "active" : ""} `}
            onClick={(e) => handleOpenBtn(e)}
          >
            <div className="avatar-user__login">
              {profileHeader?.data?.avatar ? (
                <img src={profileHeader?.data?.avatar} alt="" />
              ) : (
                <FaUserCircle />
              )}
            </div>
            <BiCaretDown />
            <ul className="btn-user__header">
              <li>
                <p onClick={() => logout()}>Đăng xuất</p>
              </li>
            </ul>
          </div>
        </div>
      </header>
    </div>
  );
};

export default Header;
