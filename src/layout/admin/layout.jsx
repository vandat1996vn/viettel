import React from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { Outlet } from 'react-router-dom';
import Spinner from '../../components/common/Spinner';

function Layout() {

  return (
    <>
      <Outlet />
      <Spinner />
    </>
  );
}

export default Layout;
