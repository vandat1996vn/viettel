import React, { useEffect, useState } from "react";
import {
  getVoiceMail,
  getVoiceText
} from "../../../features/voiceMail/voiceMailSlide";
import { getRecord, getDowAudio } from "../../../features/record/recordSlide";
import { useDispatch } from "react-redux";
import moment from "moment";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Pagination } from "../../../components/common/Pagination";
import { usePaginationState } from "../../../hooks/use-pagination";
import { FaPhoneSlash, FaPhone } from "react-icons/fa";
import Loading from "../../../components/common/Loading";
import { BiChevronDownCircle } from "react-icons/bi";
// react tabless

const VoiceMail = () => {
  const dispatch = useDispatch();

  const [recordCall, getRecordCall] = useState([]);
  const [filter, setFilter] = useState({});
  const [startDate, setStartDate] = useState(moment().toDate());
  const [phone, setPhone] = useState("");
  const [audio, setAudio] = useState([]);
  const [checkAll, setCheckAll] = useState(false);
  const [loading, setLoading] = useState(false);
  const pagination = usePaginationState();
  const inputTable = document.querySelectorAll(".check-down__record");
  const inputCheckAll = document.getElementById("input-change__all");
  const [uuidVoice, setUuidVoice] = useState();

  const [voiceText, setVoiceText] = useState();

  const [activeIndex, setActiveIndex] = useState(null);

  useEffect(() => {
    setLoading(false);
    dispatch(
      getVoiceMail({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage
      })
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);

        inputTable.forEach((item) => {
          item.removeAttribute("checked");
        });
        setAudio([]);
        setCheckAll(false);
        inputCheckAll.checked = false;
      });
  }, [pagination]);

  const handelFilter = (e) => {
    setFilter({ ...filter, [e.target.name]: e.target.value });
  };

  console.log("recordCall", recordCall);

  const titleTable = [
    "Ngày gọi",
    "Nguồn",
    "Số điện thoại",
    "Id",
    "Tài khoản",
    "Ghi âm"
  ];

  const listRecord = recordCall?.map((item) => {
    return {
      date: item.origtime,
      source: item.callerid,
      durations: item.duration,
      recording: item.record,
      mail: item.mailboxuser,
      id: item.msgId
    };
  });

  console.log("Record", listRecord);

  const handleSearch = () => {
    setLoading(false);
    dispatch(
      getVoiceMail(
        {
          ...filter,
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage
        },
        [pagination]
      )
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);
      });
  };

  const getContent = (uuidVoice) => {
    dispatch(
      getVoiceText({
        uuid: `${uuidVoice}`
      })
    )
      .unwrap()
      .then((res) => {
        setVoiceText(res?.data?.contents);
      });
  };

  console.log("UuidVoice", uuidVoice);

  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Chi tiết cuộc gọi</h3>
          <form>
            <div className="groups-filter__alls">
              <div className="groups-input__alls">
                <p className="label-input__alls">Từ ngày:</p>
                <input
                  type="date"
                  className="control-alls input-alls input-date start-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="from_dt"
                  onChange={(e) => {
                    handelFilter(e);
                    setStartDate(e.target.value);
                  }}
                />
              </div>
              <div className="groups-input__alls">
                <p className="label-input__alls">Đến ngày</p>
                <input
                  type="date"
                  className="control-alls input-alls input-date end-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="to_dt"
                  min={startDate}
                  onChange={(e) => handelFilter(e)}
                />
              </div>
              <div className="groups-input__alls">
                <input
                  type="text"
                  name="phone"
                  className="control-alls input-alls"
                  placeholder="Số điện thoại"
                  onChange={(e) => {
                    handelFilter(e);
                    setPhone(e.target.value);
                  }}
                />
              </div>
              <button
                type="submit"
                className="btn-blue__alls"
                value="Tìm kiếm"
                onClick={(event) => {
                  event.preventDefault();
                  handleSearch();
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </form>
        </div>
        {loading === true ? (
          <div className="list-record table-list__all">
            <table className="table-alls table-voice__main">
              <tbody>
                <tr>
                  {titleTable?.map((item, index) => (
                    <td key={index}>{item}</td>
                  ))}
                </tr>
                {listRecord?.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <p className="date-call">
                        {moment.unix(item.date).format("DD/MM/YYYY HH:mm:ss")}
                      </p>
                    </td>
                    <td>
                      <p>{item.source}</p>
                    </td>
                    <td>
                      <p>{item.durations}</p>
                    </td>
                    <td>
                      <p>{item.id}</p>
                    </td>
                    <td>
                      <p>{item.mail}</p>
                    </td>
                    <td>
                      <audio controls src={item.recording}>
                        <a href={item.recording}>Download audio</a>
                      </audio>
                    </td>

                    <td
                      className="btn-voice__content"
                      onClick={() => {
                        setVoiceText();
                        setActiveIndex(activeIndex === index ? null : index); // setActiveIndex push index if activeIndex === index setActiveIndex push null
                        getContent(item?.id);
                      }}
                    >
                      <p>
                        <BiChevronDownCircle />
                      </p>
                    </td>
                    {activeIndex === index && ( // if activeIndex === index render <td></td>
                      <td className="content-voice__text">
                        <p className="label-voice__text">Voice Text:</p>
                        <p className="content-voice__text">
                          {loading ? voiceText : ""}
                        </p>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <Loading />
        )}
        {loading === false ? (
          // "Không có dữ liệu"
          ""
        ) : (
          <Pagination
            currentPage={pagination.page}
            pageSize={pagination.perPage}
            lastPage={Math.min(
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              ),
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              )
            )}
            onChangePage={pagination.setPage}
            onChangePageSize={pagination.setPerPage}
            onGoToLast={() =>
              pagination.setPage(
                Math.ceil(
                  (recordCall.results ? recordCall?.count : 0) /
                    pagination.perPage
                )
              )
            }
          />
        )}
      </div>
    </>
  );
};

export default VoiceMail;
