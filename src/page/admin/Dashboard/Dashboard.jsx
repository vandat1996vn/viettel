import React, { useState, useEffect } from "react";
import {
  BiArrowBack,
  BiLineChart,
  BiData,
  BiBarChartAlt
} from "react-icons/bi";
import { getDashboard } from "../../../features/dashboard/dashboardSlice";
import { useDispatch } from "react-redux";
import Loading from "../../../components/common/Loading";
import { useSelector } from "react-redux";
import { selectProfile } from "../../../features/user/userSlice";
// chart js
import {
  Chart as ChartJS,
  Tooltip,
  ArcElement,
  Legend,
  LinearScale,
  Title,
  CategoryScale,
  BarElement
} from "chart.js";

import { Bar, Pie } from "react-chartjs-2";

const Dashboard = () => {
  const dispatch = useDispatch();
  const [infoDay, setInfoDay] = useState({});
  const [infoMonth, setInfoMonth] = useState({});
  const [loading, setLoading] = useState(false);

  const profileDashBoard = useSelector(selectProfile);

  const [directions, setDirections] = useState();

  useEffect(() => {
    dispatch(getDashboard())
      .unwrap()
      .then((res) => {
        setLoading(true);
        setInfoDay(res?.data?.day);
        setInfoMonth(res?.data?.month);
      });
  }, []);

  useEffect(() => {
    dispatch(getDashboard({ direction: directions }))
      .unwrap()
      .then((res) => {
        setLoading(true);
      });
  }, [directions]);

  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    ArcElement,
    Title,
    Tooltip,
    Legend
  );

  const data = {
    labels: [
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "11",
      "12",
      "13",
      "14",
      "15",
      "16",
      "17",
      "18",
      "19",
      "20",
      "21",
      "22",
      "23",
      "24",
      "25",
      "26",
      "27",
      "28",
      "29",
      "30",
      "31"
    ],
    datasets: [
      {
        label: "Thành công",
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.2, 0.4, 0.3, 1],
        backgroundColor: "#27A844"
      },
      {
        label: "Đã hoàn thành",
        data: [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.2, 0.4, 0.3, 1, 0, 0, 0, 0
        ],
        backgroundColor: "#DC3346"
      }
    ]
  };

  const options = {
    responsive: true,
    scales: {
      xAxes: [
        {
          gridLines: {
            display: true,
            drawBorder: false,
            borderDash: [3, 3],
            zeroLineColor: "blue"
          },
          categoryPercentage: 0.7,
          barPercentage: 0.9,
          ticks: {
            beginAtZero: true
          }
        }
      ],
      yAxes: [
        {
          display: false,
          gridLines: {
            display: false,
            zeroLineColor: "transparent"
          },
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    plugins: {
      legend: {
        display: false
      }
    }
  };

  const dataPie = {
    labels: [],
    datasets: [
      {
        label: "",
        data: [100, 5],
        backgroundColor: ["#11A4AC", "#E6E6E6"],
        borderColor: ["#FFFFFF", "#FFFFFF"],
        borderWidth: 1
      }
    ]
  };

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-greeting">
            Xin chào <b>{profileDashBoard?.data?.username}</b>
          </h3>
        </div>
        <div className="top-slogan__dashboard">
          <marquee style={{ width: "100%" }}>
            &#128293; &#128293; &#128293; Vietel thông báo quá trình dơn giản
            hóa tin nhắn zms trên toàn hệ thống &#128165; &#128165; &#128165;
          </marquee>
        </div>
        <div className="in-out__sever">
          <div className="check-box__alls">
            <input
              type="radio"
              className="form-check-input"
              onClick={() => setDirections("in")}
              id="in"
              name="direction"
            />
            <span className="checkmark"></span>
            <label>In</label>
          </div>
          <div className="check-box__alls">
            <input
              type="radio"
              className="form-check-input"
              onClick={() => setDirections("out")}
              id="out"
              name="direction"
            />
            <span className="checkmark"> </span>
            <label>Out</label>
          </div>
        </div>
        <div className="content-dashboard">
          <div className="row">
            <div className="col-lg-12">
              <div className="box-dashboard">
                <h3 className="title-box__dashboard">
                  <BiData /> SỐ LIỆU :
                </h3>
                {loading === true ? (
                  <div className="list-info__dashboard">
                    <div className="row gutter-10">
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {infoMonth?.incomingCall !== null
                              ? infoMonth?.incomingCall?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            In comming calls this month
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {infoMonth?.incomingTime !== null
                              ? infoMonth?.incomingTime?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            In comming minutes this month
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {infoMonth?.outgoingCall !== null
                              ? infoMonth?.outgoingCall?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Out going calls this month
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {infoMonth?.outgoingTime !== null
                              ? infoMonth?.outgoingTime?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Out going minutes this month
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="row gutter-10">
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {!infoDay?.incomingCall !== null
                              ? infoDay?.incomingCall?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Incoming calls today
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {!infoDay?.incomingTime !== null
                              ? infoDay?.incomingTime?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Incoming minutes today
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {!infoDay?.outgoingCall !== null
                              ? infoDay?.outgoingCall?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Outgoing calls today
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="item-info__dashboard">
                          <BiArrowBack className="icons-info__dashboard" />
                          <p className="number-item__dashboard">
                            {!infoDay?.outgoingTime !== null
                              ? infoDay?.outgoingTime?.toLocaleString("en-US")
                              : ""}
                          </p>
                          <p className="text-item__dashboards">
                            Out going minutes today
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <Loading />
                )}
              </div>
            </div>
            <div className="col-lg-8">
              <div className="box-dashboard box-chart__dashboard">
                <div className="box-dashboard__top">
                  <h3 className="title-box__dashboard">
                    <BiLineChart /> THỐNG KÊ
                  </h3>
                </div>
                <div className="box-dashboard__content">
                  <ul className="list-label__chart">
                    <li>
                      <p
                        className="label-color__chart"
                        style={{ "background-color": "#27A844" }}
                      ></p>
                      <p>Thành công</p>
                    </li>
                    <li>
                      <p
                        className="label-color__chart"
                        style={{ "background-color": "#DC3346" }}
                      ></p>
                      <p>Hoàn thành</p>
                    </li>
                  </ul>
                  <Bar
                    className="bar-chart__dashboard"
                    width=""
                    height=""
                    data={data}
                    options={options}
                  />
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="box-dashboard box-chart__dashboard">
                <div className="box-dashboard__top">
                  <h3 className="title-box__dashboard">
                    <BiBarChartAlt /> TOP SẢN LƯỢNG
                  </h3>
                </div>
                <div className="box-dashboard__content">
                  <Pie className="pie-chart__dashboard" data={dataPie} />
                  <ul className="list-label__chart">
                    <li>
                      <p
                        className="label-color__chart"
                        style={{ "background-color": "#11A4AC" }}
                      ></p>
                      <p>Hồ Chí Minh</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
