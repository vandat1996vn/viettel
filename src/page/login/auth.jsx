//react hook
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";

import { useDispatch, useSelector } from "react-redux";
import { LoginApi } from "../../features/login/authThunk";

import { useNavigate } from "react-router-dom";
import FormInput from "../../components/common/FormInput";
import "./auth.scss";
import img from "../../assets/images/robo.png";
import { login, selectAccessToken } from "../../features/login/authSlice";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import logoHeader from "../../assets/images/logo-menu.png";
import { getProfile, selectProfile } from "../../features/user/userSlice";

const Auth = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [active, setActive] = useState(false);
  const [profile, setProfile] = useState();
  // const profile = useSelector(selectProfile)

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    dispatch(
      LoginApi({
        username: data.username.trim(),
        password: data.password.trim()
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(login());
        dispatch(getProfile())
          .unwrap()
          .then((responsi) => {
            setProfile(responsi?.data);
          });
        toast.success("ĐĂNG NHẬP THÀNH CÔNG");
        console.log("tokens", res);
        localStorage.setItem("access_token", res.token);
        // if (profile?.isStaff === true) {
        //   navigate("/admin/statisticalcard");
        // }
        // if (profile?.isStaff === false) {
        //   navigate("/admin/recordcall");
        // }
        
          // setTimeout(document.getElementsByClassName("btn-login").click(),5000);
        

        
      })
      .catch((err) => {
        toast.error("ĐĂNG NHẬP THẤT BẠI");
      });
  };

  useEffect(() => {
    if (profile?.isStaff === true) {
      navigate("/admin/statisticalcard");
    }
    if (profile?.isStaff === false) {
      navigate("/admin/dashboard");
    }
  }, [profile?.isStaff]);

  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHovers
        theme="light"
      />
      <div className="content-login">
        <div className="header-form__login">
          <div className="container">
            <img src={logoHeader} alt="" />
          </div>
        </div>
        <div className="container">
          <div className="box-login">
            <div className="row gutter-0">
              <div className="col-lg-12">
                <div className="content-from__login">
                  <h3 className="title-login__form">ĐĂNG NHẬP</h3>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <FormInput
                      name="email"
                      type="text"
                      bottom="20px"
                      styleInput={{}}
                      {...register("username", {
                        required: false
                      })}
                      placeholder="Nhập tài khoản"
                      required
                    />

                    {errors?.username && (
                      <p style={{ color: "red", fontSize: "12px" }}>
                        Không thể để trống tài khoản
                      </p>
                    )}

                    <FormInput
                      name="password"
                      type="password"
                      bottom="30px"
                      styleInput={{}}
                      {...register("password", { required: false })}
                      active={active}
                      setActive={setActive}
                      required
                      placeholder="Nhập mật khẩu"
                    />
                    {errors?.password && (
                      <p style={{ color: "red", fontSize: "12px" }}>
                        Không thể để trống mật khẩu
                      </p>
                    )}
                    <div>
                      <button
                        className="btn-login"
                        style={{}}
                        type="submit"
                      >
                        Đăng nhập
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Auth;
