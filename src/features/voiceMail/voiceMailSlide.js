import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

//////initial
const initialState = {
  loading: false,
  listInfomation: {},
  urlDownload: ''
};
// Actions
const ACTION = {
  GET_VoiceMail: 'api/getVoiceMail',
  GET_VoiceText: 'api/getVoiceText',
};

export const getVoiceMail = createAsyncThunk(ACTION.GET_VoiceMail, async (body) => {
  return axiosClient.get('/cdr-btc/', { params: body });
});

export const getVoiceText = createAsyncThunk(ACTION.GET_VoiceText, async (body) => {
  return axiosClient.get(`/speed-to-text/${body.uuid}`, body);
});

const VoiceMailSlide = createSlice({
  name: 'VoiceMailSlide',
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getVoiceMail.pending, (state) => {
        state.loading = true;
      })
      .addCase(getVoiceMail.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getVoiceMail.fulfilled, (state, action) => {
        state.success = false;
        // state.ticketList = action.payload;
      })

      builder
      .addCase(getVoiceText.pending, (state) => {
        state.loading = true;
      })
      .addCase(getVoiceText.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getVoiceText.fulfilled, (state, action) => {
        state.success = false;
      })
  },
});

export const { Infomation } = VoiceMailSlide.actions;

const { reducer: VoiceMailReducer } = VoiceMailSlide;
export default VoiceMailReducer;
