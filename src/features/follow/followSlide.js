import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

//////initial
const initialState = {
  loading: false,
  listInfomation: {},
};
// Actions
const ACTION = {
  GET_FOLLOW: 'api/getFollow',
};

export const getFollow = createAsyncThunk(ACTION.GET_FOLLOW, async (body) => {
  return axiosClient.get('/connected-device/', { params: body });
});

const followSlide = createSlice({
  name: 'api',
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getFollow.pending, (state) => {
        state.loading = true;
      })
      .addCase(getFollow.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getFollow.fulfilled, (state, action) => {
        state.success = false;
      })
  },
});

export const { Infomation } = followSlide.actions;

const { reducer: followReducer } = followSlide;
export default followReducer;
