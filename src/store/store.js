import { combineReducers, configureStore } from '@reduxjs/toolkit';

import authReducer from '../features/login/authSlice';
import recordReducer from '../features/record/recordSlide';
import argenreducer from '../features/argen/argenSlide';
import followReducer from '../features/follow/followSlide';
import extOnlineReducer from '../features/extensionOnline/extensionOnline';
import userReducer from '../features/user/userSlice';
import VoiceMailReducer from '../features/voiceMail/voiceMailSlide';


const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  record: recordReducer,
  argen: argenreducer,
  follow: followReducer,
  extOnline: extOnlineReducer,
  voiceMail: VoiceMailReducer
});

const configStore = () => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false,
      }),
  });

  return store;
};

const store = configStore();
export default store;
